#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <fstream.h>
#include <unistd.h>

using namespace std;

/*
Rectangle class which describes all attributes of a 
rectangle including (x,y) left top corner coordinates
and height & length.
Also includes usual constructors, getter & setter methods
*/
class Rectangle 
{
   private :
           int x;
	       int y;
	       int length;
	       int height;
	       
   public :
	  	  
	  Rectangle()
	  {
      }
      
      Rectangle(int x1, int y1, int length1, int height1)
      {
         x = x1;
         y = y1;
         length = length1;
         height = height1;
      } 
	  
	  void setX(int xx)
	  {
          x = xx;
      }
      int getX()
      {
          return x;
      }
      int getY()
      {
          return y;
      }
      void setY(int yy)
      {
          y = yy;
      }
	  int getLength()
	  {
          return length;
      }
      void setLength(int len)
      {
           length = len;
      }
      void setHeight(int h)
      {
           height = h;
      }
      int getHeight()
      {
          return height;
      }
      
	  int intersect(Rectangle rect)
	  {
	      if(((x+length) < rect.x) || ((y+height) > y))
	         return -1;
	      else
	         return 0;
	  }
	  
	  void display(int i)
	  {
           cout<<"\n The Rectangle "<<i+1<<" is at ("<<x<<","<<y<<")"<<" with height="<<height<<" and length="<<length;
      }
};

map<int, map<int, pair<string, int> > > xmap;  //map of x vs y coordinates
map<int, map<int, pair<string, int> > > ymap;  //map of y vs x coordinates.
map<string, vector<int> > checkmap;            //map to find if a particular intersection has already been calculated.
ifstream fin;

/*
This method calculates the first point of intersection for 
the rectangles indicated by the string pattern str
Returns : pair(x,y) coordinates of first intersection.
*/
pair<int, int> calculateFirstInstance(string str)
{
   map<int, map<int, pair<string, int> > >::iterator mainiter;
   map<int, pair<string, int> >::iterator subiter;
   pair<int, int>point;
   vector<int> vec;
   
   for(int z=0;z<str.length();z++)
   {
      if(str[z] == 'y')
        vec.push_back(z);
   }
   for(mainiter=xmap.begin(); mainiter != xmap.end(); mainiter++)
   {
     for(subiter = (mainiter->second).begin(); subiter != (mainiter->second).end(); subiter++)
     {
       int flag = 1;
       string temp = (subiter->second).first;
       for(int z=0;z<vec.size();z++)
       {
          if(temp[vec.at(z)] != 'y')
          {
             flag = 0;
             break;
          } 
       }
       if(flag == 1)
       {
         point.first = mainiter->first;
         point.second = subiter->first;
         return point;
       }
     }
   }
}


/*
This method starts at theh first point of intersection and traverses 
the map landscape to find the entire size of the intersection of the rectangles
denoted by the string pattern in str.
Stores the details of the intersection in the checkmap in the following format
(nnnnyynnnynn....) -> (x, y, height, length)  //key is string & value in vector format
*/
void calculateIntersection(int x, int y, string str)
{
  string retval;
  vector<int> vec;
  int counter = 0;
  
  for(int i=0;i<str.length();i++)
  {
    if(str[i] == 'y')
    {
      vec.push_back(i);
    }
  }
    
  map<int, map<int, pair<string, int> > >::iterator iter1;
  map<int, pair<string, int> >::iterator smalliter1;
  
  iter1 = xmap.find(x);
  smalliter1 = (iter1->second).find(y);
  while(smalliter1 != (iter1->second).end())
  {
    int flag = 1;
    for(int z=0;z<vec.size();z++)
    {
      if(((smalliter1->second).first)[vec.at(z)] != 'y')
      {
        flag = 0;
        break;
      }
    }
    if(flag == 0)
      break;
    else
    {
       counter++;
       smalliter1++;
    }
  }
  vector<int> detailVec;
  detailVec.push_back(x);
  detailVec.push_back(y);
  detailVec.push_back(counter);
  checkmap[str] = detailVec;
}  
  
  
/*
Builds the map of x coordinates vs y coordinates.
Every point in the landscape is represented as a string
consisting of y's and n's. 
A 'y' at index i in the string indicates that rectangle i 
is present at that (x,y) coordinates. 
An 'n' indicates the absence of rectangle i at that location.
*/
int build_xmap(int x, int y, int rect_no)
{
    map<int, map<int, pair<string, int> > >::iterator iter;
    iter = xmap.find(x);
    if(iter == xmap.end())
    {
        map<int, pair<string, int> > tempmap;
        pair<string, int> temppair;
        string str(1000, 'n');
        str[rect_no] = 'y';
        temppair.first = str;
        temppair.second = 1;
        tempmap[y] = temppair;
        xmap[x] = tempmap;
    }
    else 
    {
         map<int, pair<string, int> >::iterator smalliter;
         smalliter = (iter->second).find(y);
         if(smalliter == (iter->second).end())
         {
            string str(1000, 'n');
            str[rect_no] = 'y';
            pair<string, int> temppair;
            temppair.first = str;
            temppair.second = 1;
            (iter->second)[y] = temppair;
         }
         else
         {
             ((smalliter->second).first)[rect_no] = 'y';
             (smalliter->second).second++;
         }
    }
}


/*
Builds the map of y coordinates vs x coordinates.
Every (y,x) point in the landscape is represented as a string
consisting of y's and n's. 
A 'y' at index i in the string indicates that rectangle i 
is present at that (y,x) coordinate. 
An 'n' indicates the absence of rectangle i at that location.
*/
int build_ymap(int y, int x, int rect_no)
{
    map<int, map<int, pair<string, int> > >::iterator iter;
    iter = ymap.find(y);
    if(iter == ymap.end())
    {
        map<int, pair<string, int> > tempmap;
        pair<string, int> temppair;
        string str(1000, 'n');
        str[rect_no] = 'y';
        temppair.first = str;
        temppair.second = 1;
        tempmap[x] = temppair;
        ymap[y] = tempmap;
    }
    else 
    {
         map<int, pair<string, int> >::iterator smalliter;
         smalliter = (iter->second).find(x);
         if(smalliter == (iter->second).end())
         {
            string str(1000, 'n');
            str[rect_no] = 'y';
            pair<string, int> temppair;
            temppair.first = str;
            temppair.second = 1;
            (iter->second)[x] = temppair;
         }
         else
         {
             ((smalliter->second).first)[rect_no] = 'y';
             (smalliter->second).second++;
         }
    }
}


/*
Function that begins traversing the entire landscape
for finding the intersections between the various rectangles.
*/
void traverseIntersections()
{
     map<int, map<int, pair<string, int> > >::iterator iter;
     map<int, pair<string, int> >::iterator smalliter;
     
     for(iter = xmap.begin();iter!=xmap.end();iter++)
     {
        map<int, pair<string, int> >::iterator indexiter = NULL;
        map<int, pair<string, int> > tempmap;
        tempmap = iter->second;
        for(smalliter = tempmap.begin();smalliter!=tempmap.end();smalliter++)
        {
           if( (smalliter->second).second > 1)
           {
                  if(checkmap.find((smalliter->second).first) == checkmap.end())
                  {
                    pair<int, int> point;
                    point = calculateFirstInstance((smalliter->second).first); 
                    calculateIntersection(point.first, point.second, (smalliter->second).first);
                  }               
           }
        }
     }
     map<string, vector<int> >::iterator checkIter;
     for(checkIter=checkmap.begin(); checkIter != checkmap.end(); checkIter++)
     {
        int length = 0;
        iter = ymap.find((checkIter->second).at(1));
        smalliter = (iter->second).find((checkIter->second).at(0));
        while(smalliter != (iter->second).end())
        {
          int flag = 1;
          for(int c=0;c<(checkIter->first).length();c++)
          {
            if((checkIter->first)[c] == 'y') 
            {
              if((smalliter->second).first[c] != 'y')
              {
                flag = 0;
                break;
              }
              
            }
          }
          if(flag == 1)
          {
             length++;
             smalliter++;
          }
          else
             break;
        }
        (checkIter->second).push_back(length);
     }
}


/*
The function to display final list of intersections 
between the various given rectangles.
*/
void displayIntersections()
{
     map<string, vector<int> >::iterator it;
     cout<<"\n\n *********** Intersection List of Rectangles *********** \n\n";
     for(it = checkmap.begin(); it!=checkmap.end(); it++)
     {
       cout<<"\n Intersection for rectangles (";
       for(int z=0; z<(it->first).length();z++)
       {
         if((it->first)[z] == 'y')
          cout<<z+1<<" ";
       }
       cout<<") is at";
       cout<<"("<<(it->second).at(0)<<","<<(it->second).at(1)<<")";
       cout<<" for height="<<(it->second).at(2)<<" and length="<<(it->second).at(3);
     }
}


/*
Main function
*/
int main()
{
  vector<Rectangle> recVec;
  string inputFile;
  string linestr;
  cout<<"\n Please enter the full path of the input file : ";
  cin>>inputFile;
  if( access(inputFile.c_str(), F_OK) == -1)
  {
      cout<<"\n Error : Invalid filename/location ";
      return 0;
  }
  fin.open(inputFile.c_str());
  while((!fin.eof()) && (recVec.size() <= 1000))
  {
     getline(fin, linestr);
     if(linestr.find("\"x\"") != string::npos)
     {
        Rectangle rect;
        int count = 0;
        char *ptr = (char *)linestr.c_str();
        ptr = strtok(ptr, ":, ");
        while(ptr)
        {
           if(atoi(ptr) != 0)
           {
             switch(count)
             {
                case 0 : 
                         rect.setX(atoi(ptr));
                         count++;
                         break;
                case 1 : rect.setY(atoi(ptr));
                         count++;
                         break;
                case 2 : rect.setLength(atoi(ptr));
                         count++;
                         break;
                case 3 : rect.setHeight(atoi(ptr));
                         count++;
                         break;
             }
           }
           ptr = strtok(NULL, ":, ");
        }
        recVec.push_back(rect);
     }
  }
  
  for(int k=0;k<recVec.size();k++)
  {
     recVec.at(k).display(k);
  }
    
  for(int k=0;k<recVec.size();k++)
  {
    Rectangle temp = recVec.at(k);
    for(int i=temp.getX();i<(temp.getX()+temp.getLength());i++)
    {
       for(int j=(temp.getY()-temp.getHeight()); j<temp.getY();j++)
       {
	      build_xmap(i, j, k);
	      build_ymap(j, i, k);
       }
    }
  }
  traverseIntersections();
  displayIntersections();
  
  system("PAUSE");
}



/*
  Rectangle rect1(15,35,15,10);
  Rectangle rect2(25,40,5,25);
  Rectangle rect3(25,30,10,10);
  */

 /*     
  Rectangle rect1(5,40,15,10);
  Rectangle rect2(15,45,15,20);
  Rectangle rect3(25,40,15,10);
  Rectangle rect4(15,35,15,15);
  Rectangle rect5(15,30,15,15);
  
  recVec.push_back(rect1);
  recVec.push_back(rect2);
  recVec.push_back(rect3);
  recVec.push_back(rect4);
  recVec.push_back(rect5);
  */
